//
//  MenuScene.swift
//  TP2
//
//  Created by GALMI AIME on 24/02/2016.
//  Copyright © 2016 GALMI AIME. All rights reserved.
//

import Cocoa
import SpriteKit

class MenuScene: SKScene {
    override func mouseDown(theEvent: NSEvent) {
        /* Called when a mouse click occurs */
        print("Click")
        if let scene = GameScene(fileNamed:"GameScene") {
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            let skView = self.view! as SKView
            skView.ignoresSiblingOrder = true
            scene.scaleMode = .AspectFill
            scene.size = skView.bounds.size
            // scene.gameWon = true
            skView.presentScene(scene)
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            
        }
    }
}
