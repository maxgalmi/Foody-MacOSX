//
//  GameScene.swift
//  TP2
//
//  Created by GALMI AIME on 04/02/2016.
//  Copyright (c) 2016 GALMI AIME. All rights reserved.
//

import SpriteKit
import AudioToolbox

let kLeftArrowKeyCode:  UInt16  = 123
let kRightArrowKeyCode: UInt16  = 124
let kDownArrowKeyCode:  UInt16  = 125
let kUpArrowKeyCode:    UInt16  = 126
let wallCategory :   UInt32 = 1
let playerCategory : UInt32 = 2
let foodCategory : UInt32  = 4
let portalCategory : UInt32 = 8
let birdCategory : UInt32 = 16
let haut = 0
let droite = 1
let bas = 2
let gauche = 3
var direction = haut
var nbfood = 5
var restants = nbfood
var nbmange = 0
var lvl = 1
var nbportals = 3
var vie = 100;


class GameScene: SKScene , SKPhysicsContactDelegate{
    override func didMoveToView(view: SKView) {
        self.physicsWorld.contactDelegate = self
        /* Setup your scene here */
        updateLabels();
        
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        let sprite = SKSpriteNode(imageNamed:"lb")
        
        sprite.setScale(0.5)
        sprite.name = "player"
        sprite.position = CGPoint(x:100, y:100);
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(25,25));
        sprite.physicsBody?.affectedByGravity = false;
        sprite.physicsBody?.categoryBitMask = 2
        
        self.addChild(sprite)
        let leftWall = SKShapeNode(rectOfSize: CGSize(width: 50, height: self.size.height));
        leftWall.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(1,self.size.height));
        leftWall.fillColor = SKColor.redColor()
        leftWall.physicsBody!.affectedByGravity = false;
        leftWall.position = CGPointMake(25, self.size.height/2);
        leftWall.physicsBody!.categoryBitMask = wallCategory;
        leftWall.physicsBody?.contactTestBitMask = playerCategory
        leftWall.physicsBody?.collisionBitMask = 0
        sprite.physicsBody?.contactTestBitMask =  foodCategory
        
        self.addChild(leftWall)
        let rightWall = SKShapeNode(rectOfSize: CGSize(width: 50, height: self.size.height));
        rightWall.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(1,self.size.height));
        rightWall.fillColor = SKColor.redColor()
        rightWall.physicsBody!.affectedByGravity = false;
        rightWall.position = CGPointMake(self.size.width - 25, self.size.height/2);
        rightWall.physicsBody!.categoryBitMask = 1;
        rightWall.physicsBody?.contactTestBitMask = 2
        rightWall.physicsBody?.collisionBitMask = 0
        rightWall.physicsBody?.contactTestBitMask = playerCategory | foodCategory | portalCategory | birdCategory
        sprite.physicsBody?.collisionBitMask = wallCategory + playerCategory + foodCategory
        
        
        
        self.addChild(rightWall)
        var posArray = Array<CGPoint>()
        var i = CGFloat(150)
        while i < self.size.width - 150 {
            var j = CGFloat(150)
            while j < self.size.height - 150 {
                posArray.append((CGPoint(x: i, y: j)))
                print("\(self.size.width) \(self.size.height)\n")
                print(" i = \(i) j = \(j)\n")
                
                
                j = j + 50
                
            }
            i = i + 50;
            print(i < self.size.width)
        }
        
        
        for _ in 1...nbfood{
            let randPos = Int(arc4random()) % posArray.count
            let food = SKSpriteNode(imageNamed:"TOM")
            food.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(25,25));
            food.position = posArray[randPos];
            
            posArray.removeAtIndex(randPos)
            food.physicsBody?.affectedByGravity = false;
            //food.fillColor = SKColor.greenColor()
            food.physicsBody?.categoryBitMask = foodCategory;
            food.physicsBody?.collisionBitMask = wallCategory + playerCategory + foodCategory
            food.physicsBody?.contactTestBitMask = playerCategory
            
            
            self.addChild(food)
            
        }
        print("Lvl :  \(lvl)")
        
        for _ in 1...lvl{
            let randPos = Int(arc4random()) % posArray.count
            let wall = SKSpriteNode(imageNamed:"bw")
            wall.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(50,50));
            wall.position = posArray[randPos];
            
            posArray.removeAtIndex(randPos)
           // wall.fillColor = SKColor.redColor()
            wall.physicsBody!.affectedByGravity = false;
            wall.physicsBody!.categoryBitMask = wallCategory;
           
            wall.physicsBody?.collisionBitMask = 0
            wall.physicsBody?.contactTestBitMask = playerCategory
            
            
            self.addChild(wall)
        }
        
        for _ in 1...lvl{
            let randPos = Int(arc4random()) % posArray.count
            let wall = SKSpriteNode(imageNamed:"bird")
            wall.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(50,50));
            wall.position = posArray[randPos];
            
            posArray.removeAtIndex(randPos)
            
            wall.physicsBody!.affectedByGravity = false;
            wall.physicsBody!.categoryBitMask = birdCategory
            wall.physicsBody?.contactTestBitMask = playerCategory
            
            
            self.addChild(wall)
        }
        
        for _ in 1...nbportals{
            let randPos = Int(arc4random()) % posArray.count
            let portal = SKSpriteNode(imageNamed:"portal")
            portal.physicsBody = SKPhysicsBody(rectangleOfSize:CGSizeMake(50,50));
            portal.position = posArray[randPos];
            
            posArray.removeAtIndex(randPos)
        
            portal.physicsBody!.affectedByGravity = false;
            portal.physicsBody!.categoryBitMask = portalCategory;
            portal.physicsBody?.contactTestBitMask = playerCategory
            
            
            self.addChild(portal)
        }
        
        
        
        
        
        // self.addChild(button)
    }
    func updateLabels(){
        let lvlLabel = childNodeWithName("level") as! SKLabelNode
        lvlLabel.text = "Niveau \(lvl)"
        lvlLabel.fontSize = 35;
        let lifeLabel = childNodeWithName("vie") as! SKLabelNode
        lifeLabel.text = "Vie \(vie)"
        lifeLabel.fontSize = 35;
        let restLabel = childNodeWithName("restant") as! SKLabelNode
        restLabel.text = "Restants \(restants)"
        restLabel.fontSize = 35;
    }
    
    override func mouseDown(theEvent: NSEvent) {
        
    }
    
    
    var playerIsMoving:Bool = false
    let d = CGFloat(0.2)
    let moveRight = SKAction.moveByX(50, y:0, duration:0.2)
    let moveLeft = SKAction.moveByX(-50, y:0, duration:0.2)
    let moveUp = SKAction.moveByX(0, y:50, duration:0.2)
    let moveDown = SKAction.moveByX(0, y:-50, duration:0.2)
    
    /* override func keyDown(theEvent: NSEvent) // A key is pressed
    {
    let player = self.childNodeWithName("player") as?SKShapeNode
    
    
    playerIsMoving = true //setting the boolean to true
    }*/
    
    
    override func keyDown(theEvent: NSEvent) {
        if let player = self.childNodeWithName("player") as?SKSpriteNode{
            //print("player trouvé")
        player.removeAllActions()
        switch theEvent.keyCode {
            
        case kLeftArrowKeyCode:
            rotatePlayer(player, new_direction: gauche)
            player.runAction(moveLeft)
            // print(1)
        case kRightArrowKeyCode:
            // print(2)
            rotatePlayer(player, new_direction: droite)
            player.runAction(moveRight)
        case kDownArrowKeyCode:
            // print(3)
            rotatePlayer(player, new_direction: bas)
            player.runAction(moveDown)
        case kUpArrowKeyCode:
            // print(4)
            rotatePlayer(player, new_direction: haut)
            player.runAction(moveUp)
        default:
            break
        }
        }
    }
    
    override func keyUp(theEvent: NSEvent)
    {
        playerIsMoving = false
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func rotatePlayer(player:SKSpriteNode,new_direction:Int){
        print(direction)
        if (direction < new_direction){
            for _ in 1...(new_direction-direction){
                let action = SKAction.rotateByAngle(CGFloat(-M_PI/2), duration:0)
                player.runAction(action)
            }
        }else if(direction > new_direction) {
            for _ in 1...(direction-new_direction){
                let action = SKAction.rotateByAngle(CGFloat(M_PI/2), duration:0)
                player.runAction(action)
            }
            
        }
        direction = new_direction
    }
    
    func playSound(file:String,withExtension:String){
        if let soundURL = NSBundle.mainBundle().URLForResource(file, withExtension: withExtension) {
                var mySound: SystemSoundID = 0
                AudioServicesCreateSystemSoundID(soundURL, &mySound)
                // Play
                AudioServicesPlaySystemSound(mySound);
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        //  print("*******************PhysicsContact********************")
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody

        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        if firstBody.categoryBitMask == playerCategory && secondBody.categoryBitMask == foodCategory {
            playSound("blop", withExtension: "wav")
            
            
       //
            secondBody.node!.removeFromParent()
            restants--
            print("Nombre manges : \(nbmange)")
         
            if(vie < 100){
                vie = vie + 1
            }
          if(restants == 0){
            if let scene = GameScene(fileNamed:"GameScene") {
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .AspectFill
                let skView = self.view! as SKView
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .AspectFill
                scene.size = skView.bounds.size
              //  scene.gameWon = true
                nbmange=0;
                lvl++;
                direction = 0;
                restants = 5
                skView.presentScene(scene)
                
            }
            }
        }
        else if firstBody.categoryBitMask == playerCategory && secondBody.categoryBitMask == portalCategory {
           // print("First contact has been made.")
            if let scene = GameScene(fileNamed:"GameScene") {
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .AspectFill
                let skView = self.view! as SKView
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .AspectFill
                scene.size = skView.bounds.size
                direction = 0;
                
                
                skView.presentScene(scene)
                
            }
        }
        else if firstBody.categoryBitMask == playerCategory && secondBody.categoryBitMask == birdCategory {
            //print("First contact has been made.")
           secondBody.node!.removeFromParent()
            if(vie <= 20){
                vie = 0
                playSound("go", withExtension: "wav")
                if let scene = GameOverScene(fileNamed: "GameOverScene"){
                    scene.scaleMode = .AspectFill
                    let skView = self.view! as SKView
                    skView.ignoresSiblingOrder = true
                    scene.scaleMode = .AspectFill
                    scene.size = skView.bounds.size
                 //   scene.gameWon = false;
                    skView.presentScene(scene)
                    vie = 100
                    lvl = 1;
                    direction = 0;
                    
                }
            }
            else{
                playSound("hit", withExtension: "wav")
                vie = vie - 20;
                
            }
            
        }
        updateLabels()
    }
    
}
