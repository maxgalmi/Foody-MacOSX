//
//  GameOverScene.swift
//  TP2
//
//  Created by GALMI AIME on 23/02/2016.
//  Copyright © 2016 GALMI AIME. All rights reserved.
//

import Cocoa
import SpriteKit

let GameOverLabelCategoryName = "gameOverLabel"


class GameOverScene: SKScene {
   /* var gameWon : Bool = false {
        // 1.
        didSet {
            let gameOverLabel = childNodeWithName("gameOverLabel") as! SKLabelNode
            gameOverLabel.text = gameWon ? "Game Won" : "Game Over"
        }
    }*/
    
    override func mouseDown(theEvent: NSEvent) {
        /* Called when a mouse click occurs */
        print("Click")
        if let scene = GameScene(fileNamed:"GameScene") {
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            let skView = self.view! as SKView
            skView.ignoresSiblingOrder = true
            scene.scaleMode = .AspectFill
            scene.size = skView.bounds.size
           // scene.gameWon = true
            skView.presentScene(scene)
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            
        }
    }
    
}
